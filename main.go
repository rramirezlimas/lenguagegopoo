package main

func main() {
	Go := &Course{
		"Go desde Cero",
		12.34,
		false,
		[]uint{12, 56, 89},
		map[uint]string{
			1: "Introduccion",
			2: "Estructuras",
			3: "Mapas",
		},
	}

	// Go.PrintClasses()
	Go.ChangePrice(67.12)
	Go.PrintClasses()
	//fmt.Println(Go.Price)
}
